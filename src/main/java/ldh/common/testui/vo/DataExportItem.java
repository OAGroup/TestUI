package ldh.common.testui.vo;

import lombok.Data;

@Data
public class DataExportItem {

    private Long id;
    private String tableName;
    private Boolean isSelected;
    private String where;
}
